/**
 * Created by emrekilic on 1/27/16.
 */

var btn = document.querySelectorAll('.btn');

function toggleClass(){
    var toggleElement = this.parentElement.children[1];
    toggleElement.classList.toggle('hide');
}

for (i=0; i<btn.length; i++){
    btn[i].addEventListener('click', toggleClass);

}
